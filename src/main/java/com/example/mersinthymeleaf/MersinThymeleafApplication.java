package com.example.mersinthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@SpringBootApplication
@Controller
public class MersinThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(MersinThymeleafApplication.class, args);
	}


	@GetMapping("")
	public String index(HttpServletRequest request, ModelMap modelMap)
	{
		return "index";
	}

	@GetMapping("find-my-city")
	public String findMyCity(HttpServletRequest request, ModelMap modelMap, @RequestParam(required = false) String key)
	{
		HashMap responseBody = new RestTemplate().getForObject(String.format("http://ip-api.com/json/%s", key), HashMap.class);
		modelMap.put("response", responseBody.get("city"));

		return "index";
	}

	@GetMapping("foo")
	public String foo(HttpServletRequest request, ModelMap modelMap)
	{
		modelMap.put("response", "foo-bar-tar");
		modelMap.put("x", "foo-bar-tar");
		modelMap.put("y", "foo-bar-tar");
		modelMap.put("z", "foo-bar-tar");
		modelMap.put("t", "foo-bar-tar");
		modelMap.put("a", "foo-bar-tar");

		return "foo";
	}

	@GetMapping("boot")
	public String boot(HttpServletRequest request, ModelMap modelMap)
	{
		return "boot";
	}


	@GetMapping("ip")
	public String ip(HttpServletRequest request, ModelMap modelMap)
	{
		String remoteAddr = request.getRemoteAddr();

		HashMap responseBody = new RestTemplate().getForObject(String.format("http://ip-api.com/json/%s", remoteAddr), HashMap.class);

		String city = (String) responseBody.get("city");

		modelMap.put("city", city);

		return "ip";
	}

}
