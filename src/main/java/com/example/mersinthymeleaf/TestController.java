package com.example.mersinthymeleaf;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("test")
    public String test(@RequestParam String route)
    {
        return String.format(Constants.HTML_CONTENT, route);
    }
}

class Constants {
    public static final String HTML_CONTENT = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Title</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "    <h1>%s</h1>\n" +
            "\n" +
            "    <img src=\"https://media.giphy.com/media/Z4NLF0caXuVYk/giphy.gif\" alt=\"\">\n" +
            "</body>\n" +
            "</html>";
}